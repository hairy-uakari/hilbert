local Turtle = require("turtle")
local turtle = nil
local called = false

function love.load()
	love.window.setMode(400, 400)
	local order = 50
	local stepSize = 2 * math.pow(2, order - 1) - 1
	print(stepSize)
	turtle = Turtle.init(400 / stepSize)
	hilbert(order, 2)
end

function love.draw()
	turtle:draw(0, 0)
end

-- 0 -> UP
-- 1 -> RIGHT
-- 2 -> DOWN
-- 3 -> LEFT
function hilbert(n, direction)
	if n == 1 then
		if direction == 2 then
			turtle:move(2)
			turtle:move(1)
			turtle:move(0)
		elseif direction == 1 then
			turtle:move(1)
			turtle:move(2)
			turtle:move(3)
		elseif direction == 3 then
			turtle:move(3)
			turtle:move(0)
			turtle:move(1)
		elseif direction == 0 then
			turtle:move(0)
			turtle:move(3)
			turtle:move(2)
		end
	else
		if direction == 0 then
			hilbert(n - 1, 3)
			turtle:move(0)
			hilbert(n - 1, 0)
			turtle:move(3)
			hilbert(n - 1, 0)
			turtle:move(2)
			hilbert(n - 1, 1)
		elseif direction == 1 then
			hilbert(n - 1, 2)
			turtle:move(1)
			hilbert(n - 1, 1)
			turtle:move(2)
			hilbert(n - 1, 1)
			turtle:move(3)
			hilbert(n - 1, 0)
		elseif direction == 3 then
			hilbert(n - 1, 0)
			turtle:move(3)
			hilbert(n - 1, 3)
			turtle:move(0)
			hilbert(n - 1, 3)
			turtle:move(1)
			hilbert(n - 1, 2)
		elseif direction == 2 then
			hilbert(n - 1, 1)
			turtle:move(2)
			hilbert(n - 1, 2)
			turtle:move(1)
			hilbert(n - 1, 2)
			turtle:move(0)
			hilbert(n - 1, 3)
		end
	end
end
