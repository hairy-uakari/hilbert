local Turtle = {}

Turtle.__index = Turtle

function Turtle.init(step)
	local this = {
		x = 0,
		y = 0,
		step = step,
		points = {{0, 0}}
	}

	setmetatable(this, Turtle)
	return this
end

function Turtle:move(direction)
	if direction == 0 then
		table.insert(self.points, {self.x, self.y - self.step})
		self.y = self.y - self.step
	elseif direction == 1 then
		table.insert(self.points, {self.x + self.step, self.y})
		self.x = self.x + self.step
	elseif direction == 2 then
		table.insert(self.points, {self.x, self.y + self.step})
		self.y = self.y + self.step
	else
		table.insert(self.points, {self.x - self.step, self.y})
		self.x = self.x - self.step
	end
end

function Turtle:draw(offx, offy)
	for i = 1, #self.points - 1 do
		love.graphics.line(self.points[i][1] + offx, self.points[i][2] + offy, 
											 self.points[i+1][1] + offx, self.points[i+1][2] + offy)
	end
end

return Turtle
